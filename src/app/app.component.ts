import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

    @ViewChild("oddContainer", {static: true}) oddContainer: ElementRef;
    @ViewChild("evenContainer", {static: true}) evenContainer: ElementRef;
    count : number;
    odds : number[];
    evens : number[];

    onTick() : void {
        this.count++;
        if ((this.count ^ 1) === (this.count + 1)) this.evens.push(this.count);
        else this.odds.push(this.count);
    }

    ngOnInit(){
        this.count = 0;
        this.odds = [];
        this.evens = [];
    }
}
