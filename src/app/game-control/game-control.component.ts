import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {





    gameStarted : boolean;

    currentTimer : ReturnType<typeof setTimeout>;

    @Output() Tick = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit(): void {
    this.gameStarted = false;
  }

  onStartGame() : void {
      this.gameStarted = true;
      this.currentTimer = setInterval(this.doTick, 1000);
  }
  onStopGame() : void {
      this.gameStarted = false;
      clearInterval(this.currentTimer);
  }

  private doTick = () => {
      if (this.gameStarted) this.Tick.emit();
  }
}

